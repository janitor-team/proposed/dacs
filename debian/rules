#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

PKG_APACHE2        := libapache2-mod-dacs
PKG_DACS           := dacs
PKG_LIBDACS        := $(shell grep -E '^Package: libdacs[0-9]+' debian/control |\
                             awk '{print $$2}')

DPKG_EXPORT_BUILDFLAGS=1
include /usr/share/dpkg/buildflags.mk


ext_lib := $(CURDIR)/ext_lib
ext_lib_mkdir = $(shell mkdir -p $(shell dirname $(ext_lib)/$(strip $(1))))
ext_lib_link =  $(shell : $(call ext_lib_mkdir, $(2)); \
                        [ -L $(ext_lib)/$(strip $(2)) ] || \
                            ln -s $(1) $(ext_lib)/$(strip $(2)) \
                 )


LIB_version_current := $(shell awk -F '=' '/^LIBCURRENT=/{print $$2}' src/configure.ac)
LIB_version_age := $(shell awk -F '=' '/^LIBAGE=/{print $$2}' src/configure.ac)
LIB_soname := $(shell expr $(LIB_version_current) - $(LIB_version_age))


override_dh_auto_configure:
	if [ ! "libdacs$(LIB_soname)" = "$(PKG_LIBDACS)" ]; then \
	   echo "Soname bump in debian/control missing; new soname: $(LIB_soname)" ;\
	   exit 1 ;\
	fi

	#Linking external libraries and programs
	# Apache
	$(call ext_lib_link, /usr/bin/apxs2, apache/bin/apxs)
	$(call ext_lib_link, /usr/share/apache2/build, apache/build)
	$(call ext_lib_link, /usr/include/apr-1.0, apache/include/apr-1)
	$(call ext_lib_mkdir, apache/conf/httpd.conf)
	touch $(ext_lib)/apache/conf/httpd.conf

	set -e ;\
	  cd src ;\
	  ./configure \
	    --prefix=/usr \
	    --localstatedir=/var \
	    --disable-prefix-check \
	    --sysconfdir=/etc/dacs \
	    --includedir=/usr/include/dacs \
	    --disable-bdb \
	    --enable-access-tokens \
	    --enable-apache-auth \
	    --enable-cas-auth \
	    --enable-cert-auth \
	    --enable-ldap-auth \
	    --enable-local-roles \
	    --enable-native-auth \
	    --enable-pam-auth \
	    --enable-token-auth \
	    --enable-unix-auth \
	    --enable-unix-roles \
	    --enable-user-info \
	    --with-htdocs=/usr/share/dacs \
	    --with-federations-root=/etc/dacs/federations \
	    --with-dacs-log=/var/log/dacs/error_log \
	    --with-ssl=/usr \
	    --with-expat=/usr \
	    --with-cgi-bin=/usr/lib/cgi-bin/dacs \
	    --with-apxs=/usr/bin/apxs2 \
	    --with-apache=$(ext_lib)/apache \
	    $(CONFFLAGS)

override_dh_auto_build:
	echo "root" > src/.ownername
	echo "www-data" > src/.groupname
	$(MAKE) -C $(CURDIR)/src
	$(MAKE) -C $(CURDIR)/apache tag
	$(MAKE) -C $(CURDIR)/src/tests

ifeq ($(filter nodoc, $(DEB_BUILD_OPTIONS)),)
	ln -s /usr/share/xml/docbook/stylesheet/nwalsh $(CURDIR)/man/docbook-xsl
	ln -s /usr/share/xml/docbook/schema/dtd/4 $(CURDIR)/man/docbook-xml
	$(MAKE) -B -C $(CURDIR)/man mansrc html index
endif

override_dh_install:
	$(MAKE) -C $(CURDIR)/src install DESTDIR=$(CURDIR)/debian/tmp
	$(MAKE) -C $(CURDIR)/apache install DESTDIR=$(CURDIR)/debian/tmp

	# misc stuff is installed via dh_installdoc and similar tools
	rm -rf $(CURDIR)/debian/tmp/usr/www/misc
	#remove broken links
	rm $(CURDIR)/debian/tmp/usr/www/man/css
	rm $(CURDIR)/debian/tmp/usr/www/man/misc
	#fix examples directory name
	mkdir -p $(CURDIR)/debian/tmp/usr/share/doc/dacs-examples
	mv $(CURDIR)/debian/tmp/usr/www/* $(CURDIR)/debian/tmp/usr/share/doc/dacs-examples
	#remove empty dirs
	rmdir $(CURDIR)/debian/tmp/usr/lib/cgi-bin/dacs-native
	rmdir $(CURDIR)/debian/tmp/usr/tmp
	#remove preformatted manpages
	rm -rf $(CURDIR)/debian/tmp/usr/share/man/cat*
	#library tweaks
	chrpath -d $(CURDIR)/debian/tmp/usr/lib/libdacs.so
	find $(CURDIR)/debian/tmp/usr/lib/cgi-bin/dacs -type f -not -name dacs_error | xargs chrpath -d
	find $(CURDIR)/debian/tmp/usr/bin -type f -not -name dacsinit | xargs chrpath -d
	chrpath -d $(CURDIR)/debian/tmp/usr/sbin/*
	rm $(CURDIR)/debian/tmp/usr/lib/libdacs.la

	dh_install

override_dh_installchangelogs:
	dh_installchangelogs HISTORY

override_dh_clean:
	[ ! -f $(CURDIR)/src/Makefile ] || $(MAKE) -C $(CURDIR)/src distclean
	[ ! -f $(CURDIR)/apache/Makefile ] || $(MAKE) -C $(CURDIR)/apache distclean

	rm -f man/docbook-xsl
	rm -f man/*.html
	rm -f man/*.man
	rm -f man/*.[0-9]

	rm -rf $(ext_lib)

	rm -rf html/handlers/dacs_pam_handler \
	   src/.build_notes \
	   src/.defaults \
	   src/config.log \
	   src/config.nice \
	   src/configure.lineno \
	   src/defs.mk \
	   src/defs.vars \
	   src/expr.loT \
	   src/libtool \
	   src/tests/Makefile \
	   tools/perl/DACScheck.pm

	rm -f src/.ownername src/.groupname

	dh_clean

%:
	dh $@ --with apache2

override_dh_missing:
	dh_missing --fail-missing
