Description: Make the build reproducible
Author: Chris Lamb <lamby@debian.org>
Last-Update: 2016-08-19

--- a/src/mkstamp
+++ b/src/mkstamp
@@ -18,7 +18,11 @@ version="$1"
 
 echo "#define DACS_VERSION_REVID \"\$Id: \$\""
 
-now=`date '+%e-%h-%Y %H:%M:%S' | sed -e "s/^ *//"`
+if test -n "$SOURCE_DATE_EPOCH"; then
+  now=`LC_ALL=C date --utc --date="@$SOURCE_DATE_EPOCH" '+%e-%h-%Y %H:%M:%S' | sed -e "s/^ *//"`
+else
+  now=`date '+%e-%h-%Y %H:%M:%S' | sed -e "s/^ *//"`
+fi
 echo "#define DACS_VERSION_DATE \"Release date $now\""
 
 echo "#define DACS_VERSION_RELEASE \"$version\""
--- a/man/mkversion
+++ b/man/mkversion
@@ -20,7 +20,11 @@ textvfile="version.txt"
 # We need the GNU cpp and this is the easiest way to ensure that
 xcpp="gcc -E"
 
-date=`date '+%e-%h-%Y' | sed -e "s/  / /"`
+if test -n "$SOURCE_DATE_EPOCH"; then
+  date=`LC_ALL=C date --utc --date="@$SOURCE_DATE_EPOCH" '+%e-%h-%Y' | sed -e "s/  / /"`
+else
+  date=`date '+%e-%h-%Y' | sed -e "s/  / /"`
+fi
 version=`echo DACS_VERSION_RELEASE \
             | $xcpp -P -include ../include/dacs_version.h - \
                 | sed -e 's/"//g' | tail -1`
--- a/src/aclcheck.c
+++ b/src/aclcheck.c
@@ -614,7 +614,11 @@ acl_index_xml_text(Ds *ds, Acl_index *ad
   int i;
   Acl_map *am;
 
-  ds_asprintf(ds, "<acl_index date_created=\"%lu\">\n", ad->date_created);
+  if (getenv("SOURCE_DATE_EPOCH")) {
+	ds_asprintf(ds, "<acl_index date_created=\"%s\">\n", getenv("SOURCE_DATE_EPOCH"));
+  } else {
+	ds_asprintf(ds, "<acl_index date_created=\"%lu\">\n", ad->date_created);
+  }
 
   for (i = 0; i < dsvec_len(ad->acl_map); i++) {
 	am = (Acl_map *) dsvec_ptr_index(ad->acl_map, i);
--- a/man/mkindex
+++ b/man/mkindex
@@ -487,7 +487,12 @@ mkdirectives >> ${f}.html
 
 mkdtd "../dtd-xsd" "/dtd-xsd" >> ${f}.html
 
-now=`date`
+if test -n "$SOURCE_DATE_EPOCH"
+then
+  now=`LC_ALL=C date --utc --date="@$SOURCE_DATE_EPOCH"`
+else
+  now=`date`
+fi
 echo '</div>' >> "${f}.html"
 echo '<div id="credit-tag">' >> "${f}.html"
 echo '<hr width="66%"><center>' >> "${f}.html"
